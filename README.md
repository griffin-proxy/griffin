# Griffin

![Griffin Logo](./logo.png "logo.png")

Backconnect proxy server in Go.

It uses MongoDB to store the proxies. That's because the filter support (not implemented yet). That can be easily changed in case you want to use another database or a simple TXT file.
