package main

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/elazarl/goproxy"
	"github.com/pkg/errors"
	"gitlab.com/griffin-proxy/griffin/pkg/middleware"
	"go.mongodb.org/mongo-driver/bson"

	mongoM "gitlab.com/griffin-proxy/griffin/pkg/mongo"
)

func getDatabaseConfig() (config map[string]map[string]string) {
	cfile, err := ioutil.ReadFile("./config/database.toml")
	if err != nil {
		log.Fatal(err)
	}
	toml.Unmarshal(cfile, &config)
	return config
}

func main() {
	var port uint

	flag.UintVar(&port, "port", 8080, "The port the server will listen to")

	dbConfig := getDatabaseConfig()

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer stop()

	mongoCli, err := mongoM.Connect(ctx, dbConfig["mongo"]["uri"])
	if err != nil {
		log.Fatal(err)
	}
	defer mongoM.Disconnect(ctx, mongoCli)

	mdb := mongoCli.Database(dbConfig["mongo"]["database"])
	col := mdb.Collection("proxies")

	filter := bson.M{}
	filter["active"] = true

	pm := middleware.NewPoolManager()

	client := &http.Client{
		Transport: &http.Transport{
			Proxy:                 pm.ProxyFunc,
			DisableKeepAlives:     true,
			ResponseHeaderTimeout: time.Duration(time.Second * 30),
		},
	}

	mitmDo := func(req *http.Request, gtx *goproxy.ProxyCtx) (*http.Request, *http.Response) {
		url := req.URL
		target := url.Hostname()
		err := pm.CreateProxies(ctx, col, target, bson.M{})
		if err != nil {
			log.Printf(
				"Unable to create pool for target %s: %+v", target,
				errors.Wrap(err, "Request Error"))
		}
		req.Close = true
		req.RequestURI = ""
		resp, err := client.Do(req)
		if err != nil {
			log.Printf("Error found: %+v", err)
			return req, resp
		}
		return req, resp
	}

	proxy := goproxy.NewProxyHttpServer()
	proxy.Verbose = true
	proxy.OnRequest().HandleConnect(goproxy.AlwaysMitm)

	proxy.OnRequest().DoFunc(mitmDo)

	server := &http.Server{Addr: fmt.Sprintf(":%d", port), Handler: proxy}

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		fmt.Println("Listening on port: ", port)
		server.ListenAndServe()
	}()

	<-ctx.Done()
	server.Shutdown(ctx)

	wg.Wait()

}
