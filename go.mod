module gitlab.com/griffin-proxy/griffin

go 1.16

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/elazarl/goproxy v0.0.0-20210110162100-a92cc753f88e
	github.com/pkg/errors v0.9.1
	go.mongodb.org/mongo-driver v1.5.1
)
