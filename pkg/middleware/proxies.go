package middleware

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"time"

	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// Proxy represents a proxy server
type Proxy struct {
	ID       uint   `json:"id"`       // A proxy ID
	List     string `json:"list"`     // The list name, ie: storm_80_main
	URL      string `json:"url"`      // The proxy URL, ie: http://167.36.78.9:6897
	Location string `json:"location"` // The proxy location, ie: de
	Type     string `json:"type"`     // A proxy type, ie: http, socks or backconnect
	Active   bool   `json:"active"`   // If it's active or not
}

type Pool <-chan *Proxy

// FetchProxies loads proxies from the DB and returns a channel of proxies
func FetchProxies(ctx context.Context, col *mongo.Collection, filter bson.M) ([]*Proxy, error) {
	proxies := make([]*Proxy, 0)
	cur, err := col.Find(ctx, filter)
	if err != nil {
		return proxies, err
	}
	defer cur.Close(ctx)
loop:
	for cur.Next(ctx) {
		proxy := &Proxy{}
		err := cur.Decode(proxy)
		if err != nil {
			log.Printf("[PROXIES] Not valid proxy?: %+v", errors.Wrap(err, "Proxy Error"))
			continue loop
		}
		log.Printf("%+v", proxy)
		proxies = append(proxies, proxy)
	}
	if err = cur.Err(); err != nil {
		return proxies, err
	}
	return proxies, nil
}

func cycle(ctx context.Context, proxies ...*Proxy) Pool {
	output := make(chan *Proxy)
	go func() {
		defer close(output)
		for {
			for _, proxy := range proxies {
				select {
				case <-ctx.Done():
					return
				case output <- proxy:
				}
			}
		}
	}()
	return output
}

// Proxies represents a pool of proxies
type Proxies struct {
	target string
	pool   Pool
}

func NewPool(ctx context.Context, col *mongo.Collection, filter bson.M) (Pool, error) {
	proxies, err := FetchProxies(ctx, col, filter)
	if err != nil {
		return nil, err
	}
	return cycle(ctx, proxies...), nil
}

// ProxyPoolManager manage a collection of pool of proxies
type ProxyPoolManager struct {
	pools map[string]*Proxies
}

func NewPoolManager() *ProxyPoolManager {
	return &ProxyPoolManager{
		pools: make(map[string]*Proxies),
	}
}

// CreateProxies creates a new pool if it does not exist
func (m *ProxyPoolManager) CreateProxies(
	ctx context.Context,
	col *mongo.Collection,
	target string,
	filter bson.M) error {
	// TODO: add option for repopulating target
	// ie: after the config's been changed
	_, ok := m.pools[target]
	if ok {
		return nil
	}
	// TODO: add filter logic: check DB config for target
	pool, err := NewPool(ctx, col, filter)
	if err != nil {
		return err
	}
	log.Printf("[PROXYPOOLMANAGER] - Created Pool for %s", target)
	m.pools[target] = &Proxies{
		target: target,
		pool:   pool,
	}
	return nil
}

// GetProxies get a pool given a target name
func (m *ProxyPoolManager) GetProxies(target string) *Proxies {
	return m.pools[target]
}

// ProxyFunc will return a new proxy with each call
func (m *ProxyPoolManager) ProxyFunc(r *http.Request) (*url.URL, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*1)
	defer cancel()
	url := r.URL
	target := url.Hostname()
	p := m.GetProxies(target)
	if p == nil {
		return nil, fmt.Errorf(
			"[PROXY MIDDLEWARE] - [%s] - Unable to get proxy: pool not found", target)
	}
	select {
	case <-ctx.Done():
		return nil, fmt.Errorf(
			"[PROXY MIDDLEWARE] - [%s] - Unable to get proxy (timeout)", target)
	case proxy := <-p.pool:
		log.Printf(
			"[PROXY MIDDLEWARE] - [%s] - Proxy found: %s", p.target, proxy.URL)
		return url.Parse(proxy.URL)
	}
}
